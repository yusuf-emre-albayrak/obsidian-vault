---
Language: "[[Python]]"
---
# Ne İşe Yarar

elemanın tipini döndürür.

---
# Örnekler

>[!example]
>```py
>x = 5
>
>type(x)
>``` 
>	 int

>[!example]
>```py
>isim = "ahmet"
>
>type(isim)
>``` 
>	str