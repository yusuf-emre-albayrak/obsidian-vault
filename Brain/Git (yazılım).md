#Nedir ?
Git, yazılım geliştirme süreçlerinde kullanılan bir [[versiyon kontrol sistemi]]dir. Git sayesinde yapacağınız projelerin adım adım versiyonlarının kopyalarını alarak daha sonra ihtiyaç duyduğunuzda aldığınız kopyalara yani versiyonlara kolayca dönebiliyorsunuz. 

![[Pasted image 20240415021137.png]]

İlk sürümü [[Linux]] çekirdeği'nin geliştirilmesinde kullanılmak üzere 2005 yılında [[Linus Torvalds]] tarafından tasarlanıp geliştirilmiş, 2019 yılı itibarıyla %70 pazar payına ulaşmıştır. [[Açık kaynak]]lı özgür bir yazılım ürünü olan Git'i istediğiniz gibi kullanabilirsiniz. 
# Git vs GitHub

**_Git_**, en yaygın olarak kullanılan **_[[versiyon kontrol sistemi|sürüm kontrol sistemi]]dir (VCS)_**. Git, dosyalarda yaptığınız değişiklikleri izler, böylece yaptığınız değişikliklerin bir kaydını tutar ve ihtiyacınız olduğunda belirli sürümlere geri dönebilirsiniz.
[[GitHub]], git kullanan projeleri yöneten bir web sitesidir. Git’i kullanmak için GitHub’a ihtiyacınız yok, ancak GitHub’ı git kullanmadan kullanamazsınız.

![](https://miro.medium.com/v2/resize:fit:854/1*tB6xbzovxMg4_eE2DCt61w.png)
***
# Avantajları

- Kod dosyalarını önceki durumlarına geri döndürebilir (Revert işlemi),
- Belirli sürelerdeki kod değişikliklerini karşılaştırabilir,
- Bir soruna veya soruna neden olabilecek bir kod parçasını en son kimin değiştirdiğini bulabilir,
- Ve belirli bir konuyu kim ve ne zaman gündeme getirdi sorusunun cevabını da bulabilirsiniz.

***
# Git kullanmadan önce bilinmesi gerekenler
Git kullanmaya başlamadan önce bazı komutların bilinmesi gerek
>[!attention] Windows 10 / 11 için ***CMD*** yerine ***Terminal*** kullanılması önerilir.

![[Terminal#Bilinmesi gereken bash komutları]]

---
# Git kullanımı

## Git kurulumu
 ilk önce sistemimizde git'in olup olmadığını kontrol etmemiz lazım
```bash
$ git --version
```

eğer hata alıyorsak buradan git'i indirmemiz lazım.[Git Download](https://git-scm.com/) veya linux ise terminal üzerinden kod ile indirilebilir:
```bash
$ sudo apt-get install git
```

eğer yardım gerekirse aşşağıdaki kod ile yardım alınabilir:
```bash
$ git --help
```

## Git güncellemek
internet sitesinden yapılabilir veya windows ise şu denenebilir:
```bash
$ git update-git-for-windows
```
***
## Git konfigürasyonu
Git'i kurduktan sonra kullanabilmemiz için konfigüre etmemiz (yapılandırmamız) gerekir. 
Bunun için **isim** ve **email** bilgilerimizi kaydetmemiz lazım. Bunun sebebi projelerde bir versiyonu kimin yayınladığını anlamak.
![[Pasted image 20240414225927.png]]
yukarıda örnekte olduğu gibi kullanılır.

### Git isim değiştirme
ilk önce ismimizi kaydedelim:
```bash
$ git config --global user.name "Yusuf Emre Albayrak"
```

### Git email değiştirme
şimdi email'i kaydedelim:
```bash
$ git config --global user.email "ysfemrealbyrk@gmail.com"
```

### Git isim ve email kontrol etme
Eğer sadece kontrol etmek istersek o zaman parametreleri yazmayız:
```bash
$ git config --global user.name
$ git config --global user.email
```

***
Şimdi burada bir ara verelim ve Git in nasıl çalıştığını anlayalım
***
# Git'i anlama

>[!attention]- DİKKAT!
>Bu konudan devam etmeden önce [[versiyon kontrol sistemi]]lerini anlamış olmanız beklenir

Git 2 farklı alanda çalışabilir ***Local*** ve ***Remote*** ancak yapılan işlemler her seferinde localden geçmek zorundadır.

>Git’in dosyalarınızın içinde bulunabileceği üç ana durum vardır: _modified_, _staged_, and _committed_:
>
>- **Modified**: dosyayı değiştirdiğinizi ama henüz veritabanına katkılamadığınızı (commit) gösterir.
  >  
>- **Staged**: değiştirilmiş bir dosyayı bir sonraki katkı pozunda (snapshot) işlenecek şekilde> işaretlediğinizi gösterir.
>
>- **Committed**: dosyanın güvenli bir şekilde yerel veritabanınızda saklandığını gösterir.

Bu da bizi bir Git projesinin üç ana bölümüne getirir: *the working tree* (çalışma ağacı),* the staging area* (izleme alanı), ve *Git klasörü*(repository).

![](https://git-scm.com/book/en/v2/images/areas.png)

**Çalışma ağacı (working tree)**, checkout komutunun projenin bir sürümünde çalıştırılmasıdır. Bu dosyalar Git dizinindeki sıkıştırılmış veritabanından çıkarılır ve sizin modifiye edebilmeniz veya kullanabilmeniz için diskinize yerleştirilir.

**İzleme alanı (staging area)** bir dosyadır, genel olarak Git klasörünüzün içindedir ve bir sonraki katkıya hangi bilgilerin işleneceğini depolar. Git terminolojisindeki teknik adı ``index``dir, ama ``izleme alanı`` ifadesi de iş görür.

**Git klasörü** ise Git’in projenize ait tüm üstverileri ve nesne veritabanını sakladığı yerdir. Bu Git’in en önemli bölümüdür; aynı zamanda da başka bir repodan _klon_ kopyaladığınızda kopyalanan şeyin de ta kendisidir.

Git’in iş akışı basitçe şöyledir:

1. Çalışma ağacında dosyaları düzenlersiniz.
    
2. ``git add …​`` komutuyla bir sonraki katkıya işlenecek olan değişiklikleri seçersiniz.
    
3. ``git commit …​`` komutuyla bir katkı işlersiniz, izleme alanındaki (stage) dosyaların pozlaını (snapshot) çeker ve Git klasörünüzde kalıcı olarak saklarsınız.
    

Eğer bir dosyanın belli bir sürümü Git klasöründeyse, o dosya _katkılanmış_ sayılır (commited). Eğer düzenlenmiş ve izleme alanına eklenmişse, _izlem_'e alınmıştır (staged). Eğer son denetlenmesinden sonra değiştirilmişse ama işlenmemişse, o halde _değiştirilmiş_ durumdadır (modified)

**Karışık gelmiş olabilir. şimdi bunu örnek vererek açıklayalım:**

test.txt isimli bir dosyamız olsun. bunu düzenlediğimiz dizin working directory oluyor. Şimdi bunu `git add` komutu ile staging area ya geçiriyoruz. peki neden böyle bir alan var çünkü test dosyamızın olduğu yerde başka dosyalarda olabilir yani bizim takip etmek istediğimiz dosyaları checkpointliyoruz. daha sonrasında commitleyip bunun snapshotunu alabiliriz. 

---
# Git Deposu
## Git Deposu Kontrolü
Öncelikle Git’i kullanmak istediğiniz dizine terminal vasıtasıyla gitmelisiniz. Git komutlarını kullanırken çalışmak istediğiniz dizinden çalışmalı ve o dizinden çıkmamalısınız.

Git'in hali hazırda deponun olup olmadığını kontrol etmek için aşağıdaki kodu yazalım:
```bash
$ git status
```
Eğer çıktı şu şekilde ise dizinde depo yoktur:
```bash
fatal: not a git repository (or any of the parent directories): .git
```
Eğer `fatal` hatası vermediyse git deposu kurulu demektir ancak biz sıfırdan başlayıp yapacağız.
## Git Deposunu Silme
1. Git deposunu silmek için deponun kurulu olduğu klasörü açın
2. Dosya gezgini ayarlarından ***Gizli Dosyaları Göster*** seçeneğini seçin.
3. " *.git* " isimli dosyayı silin.
4. Terminalde aynı dizine gidip `$ git status` komutunu çalışıtırın
5. `fatal` hata alıyorsanız depo silinmiştir.
## Git Deposu Başlatma
Projenizin bulunduğu dizine gidin:
```console
$ cd /c/user/my_project
```

Proje dizinine girdikten sonra git deposunu kurmak için şunu yazın:

```console
$ git init
```

Bu, tüm gerekli repo dosyalarını içeren `.git` adında yeni bir alt dizin oluşturur (Yani bir Git repo temeli). **Bu aşamada, projenizde henüz takip edilen bir şey yoktur.**
## Dosyaları Seçme
Şimdi dosyalarımızı stage aktarmalı yani hangi dosyamızın versiyon kontrolüne gireceğini seçmeliyiz:
İlk önce `git status` komutunu girelim:
```bash
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        test.txt

nothing added to commit but untracked files present (use "git add" to track)
```
Çıktımız bu şekilde peki ne anlama geliyor:
1. `On branch master` : *master* dalında olduğumuzu gösteriyor.
2. `No commits yet` : Daha commit atılmadığı anlamına geliyor
3. `Untracked files` : İzlenmeyen yani ***stage*** kısmında olmayan dosyaların listesini gösteriyor.
4. Bize yardımcı olmak için örnek bir `git add` komutu vermiş
5. test.txt şuan working directory de yani henüz git sistemi bunu izlemiyor.

O zaman test dosyamızı *stage*'e ekleyelim:

```bash
$ git add test.txt
```
>[!hint] 
>Siz burada *test.txt* yerine kendi dosyanızı yazabilirsiniz. Veya tüm dosyaları eklemek için
>```bash
>$ git add .
>```
>veya sonu *.txt* olan tüm dosyalar için
>```bash
>$ git add *.txt
>```

`git status` komutu ile tekrardan kontrol yapalım:
```bash
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
        new file:   test.txt
```
Çıktımı tekrardan inceleyelim:
1. 3 satır gördüğünüz gibi artık `changes to be committed` olarak değişti. yani bu artık aşağıdaki dosyaların commitlenebileceği anlamına geliyor.
2. `new file` diye bir ek geldi. eğer değişiklik olmuş olsaydı o zaman bu "+++" veya "---" olarak belli edilirdi.

## Depoya Taşıma
Dosyaların seçtiğimize göre şimdi sıra bunları depoya geçirmekte. Eğer ilk defa depoya dosya taşıyacaksak `initial commit` (ilk katkı) yapmamız lazım.
Bunun için `git commit` komutunu kullanacağız. 
Bu komutun yanı sıra `commit` komutunun mesaj seçeneğini kullanacağız ki yaptığımız versiyon kayıtlarının bir mesaj ile daha anlaşılır şekilde tutabilelim. Komutumuz da `-m` ekini kullandık ve devamında çift tırnak içinde mesajımızı yazdık. Komutumuz:
```bash
$ git commit -m "initial project version"
```
ve bu şekilde çıktı aldık:
```bash
[master (root-commit) c179d81] initial project version
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 test.txt
```

Şimdi tekrardan `git status` komutunu çalıştıralım:
```bash
On branch master
nothing to commit, working tree clean
```
gördüğünüz gibi working tree yani stage alanı temiz buda demek oluyor ki dosyamız alandan depoya gitti. Biz şimdi kodumuzda bir değişiklik yapınca aslında ilk alanda değişiklik yapmış olacağız.



# Git Kodları Hepsi

![[1_OaiXIVZeRMDc4YFXb_wUfQ.png]]

---
# Kaynaklar
https://git-scm.com/book/tr/v2
[Git Docs - https://github.com/git/git/tree/master/Documentation](https://github.com/git/git/tree/master/Documentation)
