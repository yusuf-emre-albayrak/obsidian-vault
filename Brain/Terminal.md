>[!attention] windows 10 / 11 için *CMD* yerine *Terminal* kullanılması önerilir.

Unix benzeri işletim sistemlerinde komut satırı kabukları, geliştiricilerin ve sistem yöneticilerinin günlük işlemlerini gerçekleştirmelerini sağlayan temel araçlardır. Zsh ve Bash, bu kabuklardan ikisi olup, her ikisi de kendine özgü özellikleri ve avantajlarıyla dikkat çeker 
# Zsh Nedir?

Zsh (Z Shell), Bourne Shell (sh) temel alınarak geliştirilmiş bir kabuktur. Özellikle otomatik tamamlama, zengin belirtimler, genişletilebilirlik ve dinamik yapılandırma gibi özellikleri ile öne çıkar.

# Bilinmesi gereken bash komutları

## Temel işlemler
- `ls`: **List** ten gelir. Mevcut dizindeki dosyaları listeler.
- `ls -la`: Gizli dosyaları da dahil ederek listeler.
- `pwd`: **Present Working Directory** den gelir Bulunduğunuz dizini gösterir.
- `cd <klasör>`: **Change Directory **den gelir. Belirtilen dizine gider.
- `cd ..`: Bir üst dizine çıkar.
- `mv <eski_ad> <yeni_ad>`:  **Move** dan gelir. Dosya veya klasörün adını değiştirir.
- `touch <dosya_adı>`: Yeni bir dosya oluşturur.
- `rm <dosya_adı>`: **Remove** dan gelir. Dosyayı siler.
-  `clear`: Terminal ekranını temizler.
## Klasör işlemleri
- `rm -rf <klasör_adı>`: Klasörü ve içindekileri siler.
- `rmdir <klasör_adı>`: **Remove Directory** den gelir. Klasörü siler.
- `mkdir <klasör_adı>`: **Make Directory** den gelir. Yeni bir klasör oluşturur.
