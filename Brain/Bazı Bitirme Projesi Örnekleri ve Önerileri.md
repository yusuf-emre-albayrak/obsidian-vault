[Bazı Bitirme Projesi Örnekleri ve Önerileri – Muhammet Baykara](https://muhammetbaykara.com/2018/02/11/bazi-bitirme-projesi-ornekleri-ve-onerileri/)

Bitirme tezi, lisans eğitimi boyunca elde edilen mesleki tecrübelerin en üst düzeyde yansıtılması gereken kapsamlı bir proje çalışmasıdır. Bu düşünce ile mutlak surette, yazılım mühendisliği temelleri ve yazılım tasarım mimarisi dersindeki raporlama tecrübesi, veri tabanı yönetim sistemlerindeki tasarım ve normalizasyon tecrübesi, yazılım kalite güvencesi ve testi dersindeki yazılım kalite metrikleri; optimizasyon, algoritma analizi ve yapay zeka derslerindeki tecrübelerin yansıtılması, buradaki yeteneklerin uygulanması önemlidir.

Bu yazıda yazılım mühendisliği bölümü öğrencileri için bazı final projesi örnek alanları ve proje önerileri verilecektir.

**Proje gerçekleştirilebilecek trend topic ve alanlar:**

1-Bilgi Sistemleri ve güvenliği.

2-Sosyal medya analizi.

3-Bilgi gizleme ve alternatif yöntemler.

4-İmge işleme ve içerik tabanlı görüntü erişim sistemleri.

5-Mobil uygulama geliştirme.

6-Nesnelerin İnterneti alanında bazı uygulamalar.

7-Yazılım tanımlı ağlar üzerine araştırma ve uygulamalar.

8-Bulut bilişim alanında bazı uygulamalar.

9-Saldırı tespit ve engelleme sistemleri üzerine uygulamalar.

10-Veri analizi ve veri madenciliği alanındaki uygulamalar.

 **Gerçekleştirilebilecek bazı proje önerileri:**

1- Görüntü işleme ve akıllı yöntemlerle suçlu analizinde benzer görüntülerin tespiti.

2- İçerik tabanlı görüntü erişim sistemleri ile medikal görüntü benzerlik analizi ve bilgisayar destekli tanı sistemi.

3- Felçli hastalarının rehabilitasyonu için EEG sinyallerinin gerçek zamanlı işlenmesi ve mobil uygulama geliştirilmesi.

4- Bulut bilişim tabanlı e-öğrenme sistemi tasarlanması.

5- Düzensiz hareket eden nesneleri algılama ve gerçek zamanlı olarak renk ve şekle dayalı izleme, tanıma sistemi.

6- Yüz Tanımaya Dayalı Öğrenci yoklama sistemi.

7- IOT Sistemlerin Tasarlanması Akıllı Ayna, Haberler ve Sıcaklık Tanıma Sistemi.

8- Medikal görüntülerin sınıflandırılması.

9- Sınavlara yönelik olarak mobil asistan ve deneme sınavı uygulaması.

10-Metin benzerliklerinin araştırılmasına yönelik yöntemlerin uygulanması ve karşılaştırılması.

11-Biyometrik sistemlerin incelenmesi, uygulama alanları ve analizleri.

12-Avuç içi temelli biyometrik sistem geliştirilmesi, avuç içi imgelerinin sınıflandırılması.

13-Yüz görüntülerinin sınıflandırılması, duygu-durum analizi vb.

14-Parmak izi görüntülerinin sınıflandırılması, cinsiyet-yaş analizi vb.

15-Dinamik web sayfası tasarımı (ticari boyutu da olabilecek bazı özgün projeler öğrenci ile özel paylaşılacaktır).

16-Sosyal medya analiz sistemlerinin tasarlanması ve bilgi keşif platformu.

17-Artırılmış gerçeklik temelli mobil uygulamaların çeşitli alanlarda gerçekleştirilmesi.

18-Kötücül sistemlerin tespitine yönelik uygulamalar (sınıflandırma alg. ile spam mail tespiti, smishing tespiti, phishing tespiti vb. )

19-Saldırı tespit sistemleri üzerine uygulamalar (öğrenci ile özel paylaşılacaktır).

20-Veri görselleştirme sistemleri üzerine uygulamalar.

21-Kapalı alanlarda navigasyon uygulaması.

22-Konum tabanlı reklamcılık üzerine uygulamalar.

23-Gömülü sistem uygulamaları.