1: Yapay Zeka Filmi https://flim.ai/
Film, TV, müzik videoları ve daha fazlasından ekran görüntüleri arasında arama yapmak için yapay zekayı kullanır
Genel kompozisyon, kamera açıları, aydınlatma tasarımı ve renk ilhamı için kullanışlıdır
Hikaye taslakları oluşturmaya yardımcı olur

2: Aynı Enerji https://same.energy/
Beta sürümünde aksaklık olabilir
Google'ın benzer görsellerine benzeyen ancak daha gelişmiş işlevler
Tek girişe benzer görseller bulmak için yapay zekayı kullanır
Modelleme referansı ve doku ilhamı için iyi

3: Herhangi Bir Şeyi Ara https://search.anything.io
Yapay zeka destekli görsel arama motoru
Sınırlı görsel seti ancak süper ayrıntılı aramalara izin verir
Instagram görsellerini içerir
İlham ve referans modellemek için kullanışlıdır

4: Çerçeve kümesi https://frameset.app/
Benzeri Film AI artı Her Şeyi Ara
Filmler, TV şovları, reklamlar vb. ekran görüntülerinde arama yapmak için yapay zekayı kullanır.
Kompozisyon, ışıklandırma ve kamera ilhamı için harika

5: Göz Şekeri https://eyecannndy.com/
Daha az spesifik arama seçenekleri sunar
Kamera hareketi, kompozisyon ve ışıklandırma için ilham sağlar
Yaratıcı çekimler ve film terminolojisi içerir
İyi bir genel ilham kaynağı