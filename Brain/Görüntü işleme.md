> [!Wikipedia]
> **Görüntü işleme** isim (Almanca Bildbearbeitung) ölçülmüş veya kaydedilmiş olan elektronik (dijital) görüntü verilerini, elektronik ortamda (bilgisayar ve yazılımlar yardımı ile) amaca uygun şekilde değiştirmeye yönelik yapılan bilgisayar çalışması.
>
> Görüntü işleme, verilerin, yakalanıp ölçme ve değerlendirme işleminden sonra, başka bir aygıtta okunabilir bir biçime dönüştürülmesi ya da bir elektronik ortamdan başka bir elektronik ortama aktarmasına yönelik bir çalışma olan "Sinyal işlemeden" farklı bir işlemdir.
>
> Görüntü işleme, daha çok, kaydedilmiş olan, mevcut görüntüleri işlemek, yani mevcut resim ve grafikleri, değiştirmek, yabancılaştırmak ya da iyileştirmek için kullanılır.
>
> Daha çok fotoğrafçılık ve grafik-Tasarım alanlarında kullanılır.
>
> [Wikipedia](https://tr.wikipedia.org/wiki/G%C3%B6r%C3%BCnt%C3%BC%20i%C5%9Fleme)