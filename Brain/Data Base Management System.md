**Redundancy:** storing the same data in multiple places
(Fazlalık: aynı verilerin birden fazla yerde tutulması)
***
**Functional Dependency**: Functional dependency occurs when the value of one column depends on the value of another column.
(Fonksiyonel Bağımlılık: bir sütunun değeri diğer bir sütunun değerine bağlı olduğunda fonksiyonel bağımlılık oluşur.)
***
