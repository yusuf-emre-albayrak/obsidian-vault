> [!Wikipedia]
> **Makine öğrenimi** (ML), veriden öğrenebilen ve görünmeyen verilere genelleştirebilen ve dolayısıyla açık talimatlar olmadan görevleri yerine getirebilen istatistiksel algoritmaların geliştirilmesi ve incelenmesiyle ilgilenen, yapay zekâda akademik bir disiplindir. Makine öğrenimi, bilgisayarların deneyimlerinden öğrenerek karmaşık görevleri otomatikleştirmeyi sağlayan bir yapay zeka alanıdır. Bu, veri analizi yaparak örüntüler tespit etme ve tahminlerde bulunma yeteneğine dayanır. Son zamanlarda yapay sinir ağları, performans açısından önceki birçok yaklaşımı geride bırakmayı başardı.
>
> Makine öğrenimi yaklaşımları, doğal dil işleme, bilgisayar görüşü, konuşma tanıma, e-posta filtreleme, tarım ve tıp dahil olmak üzere birçok alana uygulanmıştır. Bu teknikler, genellikle tahmine dayalı analitik olarak tanımlanan iş sorunlarına yönelik uygulamalarda önemli bir rol oynamaktadır. ML, iş sorunlarına yönelik uygulamasında tahmine dayalı analitik denir. Makine öğreniminin tümü istatistiksel temelli olmasa da, hesaplamalı istatistiksel yöntemlerinin önemli bir kaynağıdır.
>
> ML'nin matematiksel temelleri matematiksel optimizasyon (matematiksel programlama) yöntemleriyle sağlanır. Veri madenciliği, gözetimsiz öğrenme yoluyla keşifsel veri analizine (EDA) odaklanan ilgili (paralel) bir bilim dalıdır.
>
> Teorik bir bakış açısından bakıldığında, muhtemelen yüksek olasılıklı doğru (PAC) öğrenme, makine öğrenimini tanımlamak için bir çerçeve sağlar.
>
> Makine öğrenimi araştırmalarının odaklandığı konu bilgisayarlara karmaşık örüntüleri algılama ve veriye dayalı akılcı kararlar verebilme becerisi kazandırmaktır. Bu, makine öğreniminin istatistik, olasılık kuramı, veri madenciliği, örüntü tanıma, yapay zekâ, uyarlamalı denetim ve kuramsal bilgisayar bilimi gibi alanlarla yakından ilintili olduğunu gösterir.
>
> [Wikipedia](https://tr.wikipedia.org/wiki/Makine%20%C3%B6%C4%9Frenimi)