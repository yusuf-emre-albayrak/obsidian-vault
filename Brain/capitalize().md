---
Language: " [[Python]]"
---

# Ne İşe Yarar

yazıların ilk harfini büyük yapar.

---
# Örnekler

>[!example]
>``` py
>name = "yusuf emre albayrak"
>
>name.capitalize()
>``` 
>	 Yusuf emre albayrak