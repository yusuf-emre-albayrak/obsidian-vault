>[!tip] Django'nun [[virtual environment]] ile kullanılması tavsiye edilir.

# Nasıl Kurulur
```bash
python -m pip install django
```
#  Kontrol etme
 Django'nun kurulu olduğunu ve hangi sürümün yüklü olduğunu anlayabilirsiniz:
```bash
python -m django --version
```
# Proje oluşturma
```bash
django-admin startproject mysite
```
mysite yerine kendi uygulama isminizi verebilirsiniz.
>[!warning]
>Projelere yerleşik Python veya Django bileşenlerinin adını vermekten kaçınmanız gerekir. Bu, özellikle `django`(Django'nun kendisiyle çakışacak) veya `test`(yerleşik Python paketiyle çakışacak) gibi adları kullanmaktan kaçınmanız gerektiği anlamına gelir .

>[!warning]
>Kodunuzu belge kökünün **dışındaki** bir dizine koyun , örneğin `/home/mycode`.
> Bu Python kodlarından herhangi birini web sunucunuzun belge köküne koymak iyi bir fikir değildir çünkü bu, insanların kodunuzu web üzerinden görüntüleyebilme olasılığını riske atar. Bu güvenlik açısından iyi değil.

Şimdi startproject'in ne oluşturduğuna bakalım:
```
mysite/
	manage.py
	mysite/
		__init__.py
		settings.py
		urls.py
		asgi.py
		wsgi.py
```
Bu dosyalar:

- Dış `mysite/`kök dizini projeniz için bir kapsayıcıdır. Adı Django için önemli değil; *onu istediğiniz herhangi bir şeyle yeniden adlandırabilirsiniz.*
- `manage.py`: Bu Django projesiyle çeşitli şekillerde etkileşimde bulunmanıza olanak tanıyan bir komut satırı yardımcı programı. [Django-admin ve Manage.py](https://docs.djangoproject.com/en/5.0/ref/django-admin/)`manage.py` ile ilgili tüm ayrıntıları okuyabilirsiniz .[](https://docs.djangoproject.com/en/5.0/ref/django-admin/)
- İç `mysite/`dizini, projeniz için gerçek Python paketidir. Adı, içindeki herhangi bir şeyi içe aktarmak için kullanmanız gereken Python paketinin adıdır (örn. `mysite.urls`).
- `mysite/__init__.py`: Python'a bu dizinin bir Python paketi olarak değerlendirilmesi gerektiğini söyleyen boş bir dosya. Python'a yeni başlıyorsanız resmi Python belgelerinde [paketler hakkında daha fazla bilgi edinin.](https://docs.python.org/3/tutorial/modules.html#tut-packages "(Python v3.12'de)")
- `mysite/settings.py`: Bu **Django projesi için ayarlar/yapılandırma.** [Django ayarları](https://docs.djangoproject.com/en/5.0/topics/settings/) size ayarların nasıl çalıştığı hakkında her şeyi anlatacaktır.
- `mysite/urls.py`: Bu Django projesinin URL bildirimleri; Django destekli sitenizin bir "içindekiler tablosu". [URL dağıtıcısında](https://docs.djangoproject.com/en/5.0/topics/http/urls/) URL'ler hakkında daha fazla bilgi edinebilirsiniz .
- `mysite/asgi.py`: ASGI uyumlu web sunucularının projenize hizmet vermesi için bir giriş noktası. Daha fazla ayrıntı için [ASGI ile nasıl dağıtılır](https://docs.djangoproject.com/en/5.0/howto/deployment/asgi/) konusuna bakın .
- `mysite/wsgi.py`: Projenize hizmet verecek WSGI uyumlu web sunucuları için bir giriş noktası. Daha fazla ayrıntı için bkz . [WSGI ile dağıtım nasıl yapılır .](https://docs.djangoproject.com/en/5.0/howto/deployment/wsgi/)

# The development server
Django projenizin çalıştığını doğrulayalım. Henüz yapmadıysanız, dış `mysite`  dizinine geçin ve aşağıdaki komutları çalıştırın:

```bash
python manage.py runserver
```
Çıktı böyle olmalı:
```
Performing system checks...

System check identified no issues (0 silenced).

You have unapplied migrations; your app may not work properly until they are applied.
Run 'python manage.py migrate' to apply them.

April 19, 2024 - 15:50:53
Django version 5.0, using settings 'mysite.settings'
Starting development server at [http://127.0.0.1:8000/](http://127.0.0.1:8000/)
Quit the server with CONTROL-C.
```
Bu işlemin sonucunda `db.sqlite3` isimli bir database oluşturdu.

>[!danger] Bu sunucuyu üretim ortamına benzeyen herhangi bir ortamda kullanmayın. Yalnızca geliştirme sırasında kullanılmak üzere tasarlanmıştır.

Sunucu artık çalıştığına göre web tarayıcınızla [http://127.0.0.1:8000/ adresini](http://127.0.0.1:8000/) ziyaret edin. “Tebrikler!” mesajını göreceksiniz. sayfa, bir roketin kalkışıyla. İşe yaradı!

## Bağlantı noktasını değiştirme

Varsayılan olarak [`runserver`](https://docs.djangoproject.com/en/5.0/ref/django-admin/#django-admin-runserver)komut, geliştirme sunucusunu dahili IP'de **8000** numaralı bağlantı noktasında başlatır.

Sunucunun bağlantı noktasını değiştirmek istiyorsanız bunu komut satırı argümanı olarak iletin. Örneğin, bu komut sunucuyu 8080 numaralı bağlantı noktasında başlatır:

```bash
python manage.py runserver 8080
```

Sunucunun IP'sini değiştirmek istiyorsanız bunu bağlantı noktasıyla birlikte iletin. Örneğin, mevcut tüm genel IP'leri dinlemek için (Vagrant kullanıyorsanız veya çalışmanızı ağdaki diğer bilgisayarlarda göstermek istiyorsanız kullanışlıdır), şunu kullanın:

```bash
python manage.py runserver 0.0.0.0:8000
```

Geliştirme sunucusuna ilişkin tüm belgeleri referansta bulabilirsiniz [`runserver`](https://docs.djangoproject.com/en/5.0/ref/django-admin/#django-admin-runserver)
## Otomatik yenileme
Geliştirme sunucusu, gerektiğinde her istek için Python kodunu otomatik olarak yeniden yükler. Kod değişikliklerinin etkili olması için sunucuyu yeniden başlatmanız gerekmez. Ancak, **dosya ekleme gibi bazı eylemler yeniden başlatmayı tetiklemez**, bu nedenle bu durumlarda sunucuyu yeniden başlatmanız gerekir.

# Django çalışma sistemi
Bir django projesini bölümlere (django app) ayırarak modüler şekilde geliştirebiliyoruz. Bu sayede proje çok büyümüş olsa bile projenin görevlerini başka bölümlere verdiğimizden dolayı karmaşıklıktan uzak bir geliştirme ortamına sahip oluyoruz.
![[Pasted image 20240423130908.png]]

Django app proje mantığını daha iyi anlayabilmek için örneğin Google firmasını örnek verebiliriz. Google anasayfasına girdiğimizde firmanın bir çok alt projesi olduğunu görebiliyoruz. Örneğin; Images, Videos, News...

Ya da bir eticaret projesi geliştirdiğimi düşündüğümüzde projemizi users, blog, admin, catalog şeklinde ayrı uygulamalara ayırabiliriz.

# App ekleme
Artık ortamımızda  - bir "proje" - kurulduğuna göre, iş yapmaya başlamaya hazırız.
Django'da yazdığınız her uygulama, belirli bir kuralı izleyen bir Python paketinden oluşur. Django, bir uygulamanın temel dizin yapısını otomatik olarak oluşturan bir yardımcı programla birlikte gelir; **böylece dizin oluşturmak yerine kod yazmaya odaklanabilirsiniz**:
```bash
python manage.py startapp newapp
```
  - burada gördüğünüz gibi `startapp` ibaresini kullandık ilk başta ise `startproject` ibaresini kullanmıştık.
<br>
Bu , şu şekilde düzenlenmiş bir dizin oluşacaktır :
```
newapp/
    __init__.py
    admin.py
    apps.py
    migrations/
        __init__.py
    models.py
    tests.py
    views.py
``` 

## Yeni app'i tanıtmak
projemize bu yeni app i tanıtmak için projemizin dizini altındaki `settings.py` içerisindeki şu kısma şu şekilde ekliyoruz.
```
INSTALLED_APPS = [
	'newapp'    #  <-----------------------| bu şekilde ekledik
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]
```
# App views sistemi
![[djangoviewurlsschema|1000]]
Django appleri birbirine `urls` ler ile bağlanır. Normalde django yeşil yolu önerir lakin biz daha rahat olduğundan her appin dizininde yeni bir `urls` oluşturup onları global `urls` e bağlayacağız.

Yeni oluşturduğumuz `urls` in içine bağzı kodları yazmamız lazım:
```python
from django.urls import path
from . import views
```
ilk satırda path i import etmemizin sebebi ana `urls` içinde olan kodda olduğu gibi urlspattern ile appleri (sayfaları) birbirine linklemek.
```python
urlpatterns = [
    path('admin/', admin.site.urls),
]
```
---
## ***şimdi  örnek yapalım***
### yeni `urls` oluşturma
yukarıdaki gibi `urls.py` diye yeni bir dosya oluşturalım ve from ile başlayan kodları yazalım
### basit response verme
appimiz views dosyasının içine bu kodları yazalım:
```python
from django.http import HttpResponse
def index(request):
    return HttpResponse("Hello World!")
```
bu kod sayesinde basit bir response oluşturuyoruz.
### kendi `urls.py` ile `views.py` bağlama
şimdi kendi `urls.py` ile `views.py` dosyamızı bağlayabiliriz. bunu için kendi `urls.py` dosyamızın içine 
```python
urlpatterns = [
    path('', views.index, name="index"),
]
```
bu kodu ekliyoruz. burada `path('',` olarak yazmamızın sebebi zaten bu `urls` ile `view` aynı yerde olması. ikinci kısımda `index`fonksiyonunu seçtik. **üçüncü kısımda ise buna `index` ismini verdik bu bize ileride bu path'e ulaşmamızı kolaylaştıracak.**
### global `urls.py` de tanıtma
şimdi kendi `urls.py` dosyamızı global `urls.py` dosyasına bağlayalım bunu yapmak için ise global `urls.py` dosyamızda şu kodları yazıyoruz:
```python
from django.urls import path, include
#------------------------------^------ bu kısmı ekledik
urlpatterns = [
    path("firstApp", include("firstApp.urls")),
    #-----1^-----------2^-------3^-------------
    #1.appi linkledik
    #2.include ile app içindeki urls alıcaz
    #3.burada linkleme işlemi bitiyor    
    path('admin/', admin.site.urls),
]
```

>[!info] Diğer URL kalıplarını eklediğinizde her zaman `include()` işlevini kullanmalısınız. `admin.site.urls` bunun tek istisnasıdır.
### çalıştırma
serverı çalıştıralım ve sayfamızı açalım.
```python
python manage.py runserver
```
![[Pasted image 20240423143138.png]]
***Bu hatayı aldık, peki neden?***
burada görüldüğü gibi global `urls` te herhangi bir `view.py` bağlı değil onun yerine `urls.py` bağlı bizim `http://127.0.0.1:8000/` değil `http://127.0.0.1:8000/firstApp` e gitmemiz lazım.
Evet gördüğünüz gibi çalıştı :D
### ekstra
peki nasıl `http://127.0.0.1:8000/` buraya gidildiğinde `firstApp` i çağırabiliriz. çünkü burası ana sayfamız olabilir.
bunun için
```python
path("firstApp", include("firstApp.urls"))
```
kısmında `firstApp` kısmını boş bırakmamız yeter.
# 404 Error raislemek
```python
from django.http import Http404
def index(request):
    try:
        pass
    except:
        raise Http404("404\nOOPS! page not found")
```
# Django MVC
Django [[MVC]] dediğimiz bir sistem ile çalışır. 
![[MVCforDjango|1000]]
# dinamik rooting
mesela kurs sayfalarımız olsun bunları tek tek tanımlamak yerine tek bir dictionary ye atıp daha sonrasında bunu bir fonksiyon ile kontrol edip döndürebiliriz
## dict oluşturma
`firstApp` içerisindeki `views.py` dosyasına bunları ekleyelim:
```python
courses = {
    "python": "python course page",
    "java" : "java course page",
    "kotlin" : "kotlin course page"
}
```
## dict fonksiyonu
daha sonra bu dicti çağırmak için aynı yerde bir fonksiyon yazalım:
```python
def course(request, item):
    return HttpResponse(courses.get(item, "Not Found!"))
```
## linkleme
şimdi bunu linkeleyelim yine `firstApp`in içinde olan `urls.py` dosyasının pathine şunu ekleyelim
```python
path("<str:item>/", views.course, name="course")
#-----1^-------2^-------
#1. < > arasında yazarak str: ile değişkeni verdik
#2. / bunu yazmak ise hata verebilir
```

# Debug mod
Proje üzerinde çalışırken debug mod açılmalıdır. eğer canlıya çekilecek ise kapanmalıdır
bunun ayarı proje dosyası içerisindeki `settings.py` dosyasındadır.
```python
DEBUG = False
#-------^ 
ALLOWED_HOSTS = ["127.0.0.1"]
```
>[!attention] Debug mod off ise allowed host verilmesi gerekir. eğer açık ise gerek yoktur.

# Template 

## Django render nedir?
`httpresponse` ile sayfada response çevirmeyi öğrendik ancak sayfaya gösterme işlemi yapmayı da öğrenmemiz lazım bunun için `render`  sınıfını kullanırız. 
peki `render` nasıl kullanılır return ederken render döndürülür.
```python
return render(request, "index.html")
```

## Template klasörü oluşturma
ancak index dosyamız yok peki ne yapacağız bu durumda ana projemizin kurulu olduğu dizinde yeni bir `templates` klasörü açalım ve içine mainpage isimli bir klasör koyalım
```
myProject/
	firstApp/
	myProject/
	db.sqlite3
	manage.py
	
	templates/   <---- bunu oluşturuyoruz.
		mainpage/   <----- bunu oluşturduk sebebini ilerde görcez
			mainpage.html   <---- html oluşturduk.
```
şimdi bu işlemde bittiğine göre `mainpage.html` içini yazalım:
```html
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial scale=1.0">
    <title>main-page</title>
</head>
<body>
    <h1>Main Page</h1>
    <h2>test test</h2>
</body>
</html>
```
şimdi bunu `views.py` da index içinde renderlayalım:
```python
def index(request):
    return render(request,"mainpage/mainpage.html")
```
`urls.py` ın böyle olduğuna emin olalım:
```python
urlpatterns = [ path('', views.index, name="index") , ]
# -------------------^------ bu kısım boş olması lazım
```
şimdi çalıştıralım ve bakalım:
**HATA!** aldık sebebi nedir?
çünkü biz template klasörünü django ya tanıtmadık bunun tanıtılması lazımdı o zaman tanıtalım:
## Template klasörü tanıtma
### Yol 1
`settings.py` içinde Templates kısmında değişiklik yapmamız lazım:
```python
import os # 1 <--------------------

TEMPLATES = [
	{
		'BACKEND':'django.template.backends.django.DjangoTemplates',
        'DIRS': [ os.path.join(BASE_DIR,"templates/") ] # 2 <------------
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',  
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
```
### Yol 2
django'nun yeni sürümlerinde artık bu şekilde yapmayı destekliyor.
```python
TEMPLATES = [
	{
		'BACKEND':'django.template.backends.django.DjangoTemplates',
        'DIRS': [ BASE_DIR,"templates/" ] # <------------
        'APP_DIRS': True,
        ...
```

# Veritabanı Kurulumu (migrate)
***Aslında bu yapılması gereken ilk işlerden biridir. ancak biz biraz erteledik.*** 

>[!attention] Dosyanın en başındaki INSTALLED_APPS ayarına dikkat edin. Projenizde kullandığınız uygulamaları tutar. Yeni bir uygulama kullanacağınız zaman bu listeye eklemelisiniz.

Varsayılan olarak gelen uygulamalar şunlardır:
```python
django.contrib.admin #--> Yönetici panelini oluşturur.
django.contrib.auth #--> Bir kimlik doğrulama sistemi.
django.contrib.contenttypes #--> İçerik türleri için bir framework.
django.contrib.sessions #--> Bir oturum frameworku
django.contrib.messages #--> Bir mesajlaşma frameworku
django.contrib.staticfiles #--> Statik dosyaları yönetmek için bir framework.
```
Bu uygulamalardan bazıları en az bir veritabanı kullanıyor. Ancak bu veritabanlarının kullanılabilmesi için bir tablo oluşturmalıyız. O tablo da şu komutla oluşuyor:
```bash
python manage.py migrate
```
şimdi uygulamamızı buna tanıtalım:
ilk önce tanıtacağımız uygulamanın `apps.py` dosyasının içindeki `name` değişkenini, içinde olduğu `class`ın ismi ile alltaki gibi `settings.py` içine ekleyin:
```python
# === apps.py ===
class FirstappConfig(AppConfig):
# ------^------- işte bu class ismi
	default_auto_field = 'django.db.models.BigAutoField'
    name = 'firstApp'
```

```python
# === settings.py ===
INSTALLED_APPS = [
    'firstApp.apps.FirstappConfig', # <------- işte buradaki gibi ekledik.
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]
```
*alttaki başlıkta devam ediyoruz...*
## model güncellemek

>[!attention] Model de her değişiklik yaptığımızda bunu yapmamız lazım.

```shell
python manage.py makemigrations [app_name]
```
# Jinja





---
# Sözlük# [doc](https://docs.djangoproject.com/en/5.0/glossary/#glossary)

**concrete mode - somut model**
Soyut olmayan ( [`abstract=False`](https://docs.djangoproject.com/en/5.0/ref/models/options/#django.db.models.Options.abstract "django.db.models.Options.abstract")) bir model.

**field - alan**
[Bir modeldeki](https://docs.djangoproject.com/en/5.0/glossary/#term-model) bir nitelik ; belirli bir alan genellikle doğrudan tek bir veritabanı sütununa eşlenir.
Bkz . [Modeller](https://docs.djangoproject.com/en/5.0/topics/db/models/) .

**generic view - genel görünüm**
Görünüm geliştirmede bulunan ortak bir deyim veya modelin soyut/genel uygulamasını sağlayan üst düzey bir [görünüm işlevi.](https://docs.djangoproject.com/en/5.0/glossary/#term-view)
Bkz. [Sınıf tabanlı görünümler](https://docs.djangoproject.com/en/5.0/topics/class-based-views/) .

**model**
Modeller uygulamanızın verilerini saklar.
Bkz . [Modeller](https://docs.djangoproject.com/en/5.0/topics/db/models/) .

**MTV**
“Model-şablon-görünümü”; MVC'ye benzer tarzda bir yazılım modeli, ancak Django'nun işleri yapma biçimini daha iyi tanımlıyor.
[SSS girişine](https://docs.djangoproject.com/en/5.0/faq/general/#faq-mtv) bakın .

**MVC**
[Model görünümü denetleyicisi](https://en.wikipedia.org/wiki/Model-view-controller) ; bir yazılım modeli. Django [bir dereceye kadar MVC'yi takip ediyor](https://docs.djangoproject.com/en/5.0/faq/general/#faq-mtv) .

**project**
Bir Django örneğinin tüm ayarlarını içeren bir Python paketi, yani bir kod dizini. Buna veritabanı yapılandırması, Django'ya özgü seçenekler ve uygulamaya özel ayarlar dahildir.

**property**
Aynı zamanda "yönetilen özellikler" olarak da bilinir ve Python'un 2.2 sürümünden beri sahip olduğu bir özelliktir. Bu, kullanımı öznitelik erişimine benzeyen ancak uygulanması yöntem çağrılarını kullanan öznitelikleri uygulamanın düzgün bir yoludur.
Görmek [`property`](https://docs.python.org/3/library/functions.html#property "(Python v3.12'de)").

**queryset - sorgu kümesi**
Veritabanından alınacak bazı satır kümelerini temsil eden bir nesne.
Bkz. [Sorgulama yapma](https://docs.djangoproject.com/en/5.0/topics/db/queries/) .

**slug**
Bir şey için yalnızca harf, sayı, alt çizgi veya kısa çizgi içeren kısa etiket. Genellikle URL'lerde kullanılırlar. Örneğin, tipik bir blog girişi URL'sinde:
[https://www.djangoproject.com/weblog/2008/apr/12/](https://www.djangoproject.com/weblog/2008/apr/12/) **bahar** /
son bit ( `spring`) sümüklü böcektir.

**template - şablon**
Verileri temsil etmek için biçimlendirme işlevi gören bir metin yığını. Bir şablon, verilerin sunumunu verilerin kendisinden soyutlamaya yardımcı olur.
Bkz. [Şablonlar](https://docs.djangoproject.com/en/5.0/topics/templates/) .

**view**
Bir sayfanın oluşturulmasından sorumlu bir işlev.