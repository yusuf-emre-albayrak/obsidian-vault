---
aliases:
  - sürüm kontrol sistemi
  - version control system
  - vcs
---

Versiyon kontrol sistemleri, kodda yapılan değişikliklerin kaydını tutarak dosyalarda yapılan değişiklikleri kaydetmeye yardımcı olan bir yazılım aracıdır.

Projede yapılan değişikleri iptal etmemize ve geri almamıza olanak sağlar. Mesela bir kod yazdınız eminsiniz o an ama başka bir arkadaşınız bunun böyle olmaması gerektiğini söyledi ancak bunu proje değişiklikleri yapıldıktan sonra söyledi. Bunları görerek geri alabiliriz. Ayrıca projede yer alan değişiklerin ne zaman , kim tarafından , neresinin değiştiğini görebiliyoruz

![[Pasted image 20240415030009.png]]

# **VCS faydaları**

- Hızlı proje geliştirmek ve ürün elde etmemizi sağlar
- Bizlere eş zamanlı geliştirme yapabilmek için olanak sunar
- Her değişiklik için görüntülenebilirlik ve hataları minimuma çekmemizi sağlar
- Proje çalışan kişiler ile paylaşılabilir ancak doğrulanmadıkça(merge olmadıkça)ana proje dosyası ile birleştirilmez
- Yukarıda yazdığım gibi ne zaman , kim tarafından , nerede , nasıl bir değişiklik yapıldığını kolayca görebilme imkanı verir bizlere

# **VCS türleri**

## **_Yerel Versiyon Kontrol Sistemi(CVS,SVN)_**

Kullanıcının çalışma projesinde yaptığı değişiklikler kendi local’i üzerinde veri tabanında tutulur. Örneğin benim bilgisayarım üzerinde bir kod yazdım ve bunu kendi bilgisayarımda tuttum. Burda sorun t anında yapılan bir değişikliği görmek için o ana kadar yapılan tüm değişikleri projeye eklemek gerekir. Yerel versiyon kontrol sistemi local tarafında gerçekleştiği için diğer geliştiriciler ile iş geliştirmek çok zordur.

![|400](https://miro.medium.com/v2/resize:fit:854/1*d9M_MBivGkJIf8lqVUlQ5w.png)

## **_Merkezi Versiyon Kontrol Sistemi(ClearCase_**)

Proje dosyasındaki tüm değişiklikler tek bir sunucuda tutulur. Bu sayede birden fazla kişinin erişimine olanak sağlar ve herkesin projede geliştirme yapması önemli bir artıdır. Yaptığımız değişikliklerin görülmesi için commit atmanız gerekmektedir. Burdaki sorun ise tüm bilgi tek bir sunucu üzerinde tutulmasıdır yani bu sunucunun başına gelebilecek en ufak bir sorunda projeyi kaybedebiliriz.

![|400](https://miro.medium.com/v2/resize:fit:854/1*SDj9TdcYCPGuH7ZcB6SJbg.png)

## **_Dağıtılmış Versiyon Kontrol Sistemi(Git, Distributed)_**

Bu VCS ‘ de birden fazla repository(Kaynak kodların bulunduğu depo) yer almaktadır. Her kullanıcının kendine ait repository ’si bulunmaktadır. Burada sunucuda herhangi bir problem olması sonucunda kullanıcılardan herhangi birininin geri yüklemesi ile projenin geri getirilmesi mümkündür.

![](https://miro.medium.com/v2/resize:fit:509/1*2cG4BW2m1E-EcklOLG51jg.png)

***
# İlgili Sayfalar
[[Git (yazılım)]]
[[GitHub]]