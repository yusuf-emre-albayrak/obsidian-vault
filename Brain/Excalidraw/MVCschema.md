---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
Model ^SWvG179c

View ^cpDkmyZO

Controller ^xp4maFZU

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/2.1.4",
	"elements": [
		{
			"type": "rectangle",
			"version": 110,
			"versionNonce": 2009724246,
			"isDeleted": false,
			"id": "qsR7bk_9exyLXilB8hDnF",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -206.4869384765625,
			"y": -261.65470123291016,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#b2f2bb",
			"width": 177.049072265625,
			"height": 76.22952270507812,
			"seed": 949979914,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"type": "text",
					"id": "SWvG179c"
				},
				{
					"id": "a2jgeEarVvayzajE1Fq8r",
					"type": "arrow"
				},
				{
					"id": "ORluOc3LShFKbUfzSaLtO",
					"type": "arrow"
				}
			],
			"updated": 1713874368212,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 64,
			"versionNonce": 733838602,
			"isDeleted": false,
			"id": "SWvG179c",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -144.95237731933594,
			"y": -236.0399398803711,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 53.979949951171875,
			"height": 25,
			"seed": 1416814614,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713874325931,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Model",
			"rawText": "Model",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "qsR7bk_9exyLXilB8hDnF",
			"originalText": "Model",
			"lineHeight": 1.25
		},
		{
			"type": "rectangle",
			"version": 302,
			"versionNonce": 1763908246,
			"isDeleted": false,
			"id": "5QDGeVx4mw-kClyQJTr6_",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -351.6180419921875,
			"y": -127.75304412841797,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffc9c9",
			"width": 177.049072265625,
			"height": 76.22952270507812,
			"seed": 832800842,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"type": "text",
					"id": "cpDkmyZO"
				},
				{
					"id": "a2jgeEarVvayzajE1Fq8r",
					"type": "arrow"
				},
				{
					"id": "q1USvpL6j_QWypem9q1E0",
					"type": "arrow"
				}
			],
			"updated": 1713874410250,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 265,
			"versionNonce": 709842902,
			"isDeleted": false,
			"id": "cpDkmyZO",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -282.01348876953125,
			"y": -102.1382827758789,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 37.8399658203125,
			"height": 25,
			"seed": 1767894794,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713874410250,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "View",
			"rawText": "View",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "5QDGeVx4mw-kClyQJTr6_",
			"originalText": "View",
			"lineHeight": 1.25
		},
		{
			"type": "rectangle",
			"version": 372,
			"versionNonce": 1526993482,
			"isDeleted": false,
			"id": "yDfenFdABsM8jFLjdCEo1",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -67.208251953125,
			"y": -125.17925262451172,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#a5d8ff",
			"width": 177.049072265625,
			"height": 76.22952270507812,
			"seed": 513827146,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"type": "text",
					"id": "xp4maFZU"
				},
				{
					"id": "ORluOc3LShFKbUfzSaLtO",
					"type": "arrow"
				},
				{
					"id": "q1USvpL6j_QWypem9q1E0",
					"type": "arrow"
				}
			],
			"updated": 1713874429400,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 339,
			"versionNonce": 1780431114,
			"isDeleted": false,
			"id": "xp4maFZU",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -25.853668212890625,
			"y": -99.56449127197266,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 94.33990478515625,
			"height": 25,
			"seed": 861513738,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713874429400,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Controller",
			"rawText": "Controller",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "yDfenFdABsM8jFLjdCEo1",
			"originalText": "Controller",
			"lineHeight": 1.25
		},
		{
			"type": "arrow",
			"version": 375,
			"versionNonce": 474186582,
			"isDeleted": false,
			"id": "a2jgeEarVvayzajE1Fq8r",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -274.39866788322263,
			"y": -137.88419342041018,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 60.534684728925725,
			"height": 90.57377624511716,
			"seed": 2140022358,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1713874428284,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "5QDGeVx4mw-kClyQJTr6_",
				"focus": -0.17851057118267755,
				"gap": 10.131149291992216
			},
			"endBinding": {
				"elementId": "qsR7bk_9exyLXilB8hDnF",
				"focus": 0.3953483820721101,
				"gap": 7.377044677734403
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					8.895310949628879,
					-81.96722412109372
				],
				[
					60.534684728925725,
					-90.57377624511716
				]
			]
		},
		{
			"type": "arrow",
			"version": 439,
			"versionNonce": 547850186,
			"isDeleted": false,
			"id": "ORluOc3LShFKbUfzSaLtO",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 26.677739163875295,
			"y": -131.32682037353516,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 44.64008047246908,
			"height": 89.75410461425781,
			"seed": 842969162,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1713874429401,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "yDfenFdABsM8jFLjdCEo1",
				"focus": 0.08597470645426106,
				"gap": 6.1475677490234375
			},
			"endBinding": {
				"elementId": "qsR7bk_9exyLXilB8hDnF",
				"focus": -0.319487938541283,
				"gap": 11.475524902343722
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-4.4761400427815445,
					-81.55738830566406
				],
				[
					-44.64008047246908,
					-89.75410461425781
				]
			]
		},
		{
			"type": "arrow",
			"version": 614,
			"versionNonce": 1762368842,
			"isDeleted": false,
			"id": "q1USvpL6j_QWypem9q1E0",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -77.79846191406251,
			"y": -91.74667316635019,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 88.93438720703124,
			"height": 1.734363836936211,
			"seed": 1557997974,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1713874429401,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "yDfenFdABsM8jFLjdCEo1",
				"gap": 10.5902099609375,
				"focus": 0.1626183932096791
			},
			"endBinding": {
				"elementId": "5QDGeVx4mw-kClyQJTr6_",
				"gap": 7.83612060546875,
				"focus": 0.037779084382163444
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-88.93438720703124,
					1.734363836936211
				]
			]
		}
	],
	"appState": {
		"theme": "dark",
		"viewBackgroundColor": "transparent",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "#a5d8ff",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 455.05548095703125,
		"scrollY": 335.5404357910156,
		"zoom": {
			"value": 2
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%