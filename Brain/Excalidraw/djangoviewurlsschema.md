---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
Django Project ^nV0uEPp2

Blog app ^lYzw28Yn

Friends app ^zpglfGun

settings.py ^LMGjfbGr

urls.py ^TtPBKwYC

views.py ^8sF27Z48

views.py ^oTqH8ZRX

urls.py ^lLotnkwY

urls.py ^Sq6EZcxf

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/2.1.4",
	"elements": [
		{
			"id": "nV0uEPp2",
			"type": "text",
			"x": -295.32455825805664,
			"y": -104.1792984008789,
			"width": 147.6798858642578,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 362228502,
			"version": 168,
			"versionNonce": 1374010646,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713869381251,
			"link": null,
			"locked": false,
			"text": "Django Project",
			"rawText": "Django Project",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Django Project",
			"lineHeight": 1.25
		},
		{
			"id": "saKJc2whkXn23ykuVD-Fx",
			"type": "rectangle",
			"x": -305.1934242248535,
			"y": -50.179298400878906,
			"width": 159.83609008789062,
			"height": 79.50820922851562,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"seed": 1275723926,
			"version": 316,
			"versionNonce": 1982908234,
			"isDeleted": false,
			"boundElements": [
				{
					"type": "text",
					"id": "lYzw28Yn"
				},
				{
					"id": "L5auT1iYXSWw9WO34OL46",
					"type": "arrow"
				}
			],
			"updated": 1713869381251,
			"link": null,
			"locked": false
		},
		{
			"id": "lYzw28Yn",
			"type": "text",
			"x": -267.25534439086914,
			"y": -22.925193786621094,
			"width": 83.95993041992188,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1710288598,
			"version": 220,
			"versionNonce": 1469688406,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713869381251,
			"link": null,
			"locked": false,
			"text": "Blog app",
			"rawText": "Blog app",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "saKJc2whkXn23ykuVD-Fx",
			"originalText": "Blog app",
			"lineHeight": 1.25
		},
		{
			"type": "rectangle",
			"version": 386,
			"versionNonce": 1206734346,
			"isDeleted": false,
			"id": "dvUGt3QLEYjoNiExLJohc",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -302.51521134955215,
			"y": 61.21993039025847,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 165.57379150390625,
			"height": 94.2623291015625,
			"seed": 1402996682,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"type": "text",
					"id": "zpglfGun"
				},
				{
					"id": "CpmVXFxfAu3B1v_OgHWUU",
					"type": "arrow"
				}
			],
			"updated": 1713869381251,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 305,
			"versionNonce": 1927147414,
			"isDeleted": false,
			"id": "zpglfGun",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -274.74826646429824,
			"y": 95.85109494103972,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 110.03990173339844,
			"height": 25,
			"seed": 648052362,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713869381251,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Friends app",
			"rawText": "Friends app",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "dvUGt3QLEYjoNiExLJohc",
			"originalText": "Friends app",
			"lineHeight": 1.25
		},
		{
			"id": "LMGjfbGr",
			"type": "text",
			"x": -48.668827056884766,
			"y": -104.96619415283203,
			"width": 103.71987915039062,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 210930314,
			"version": 169,
			"versionNonce": 636157130,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713869381251,
			"link": null,
			"locked": false,
			"text": "settings.py",
			"rawText": "settings.py",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "settings.py",
			"lineHeight": 1.25
		},
		{
			"id": "TtPBKwYC",
			"type": "text",
			"x": 132.38036727905273,
			"y": -104.99893951416016,
			"width": 60.7999267578125,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 350555030,
			"version": 197,
			"versionNonce": 1072139478,
			"isDeleted": false,
			"boundElements": [
				{
					"id": "W7rm0PpQ8eDvXOs6NEpW7",
					"type": "arrow"
				},
				{
					"id": "S-VUP-RJ3BQRTi4ZuSgz2",
					"type": "arrow"
				},
				{
					"id": "qzmpGRUcyrYYRqkCjB8ZN",
					"type": "arrow"
				},
				{
					"id": "2Jsg1BMtcjlpaWqUYdnRv",
					"type": "arrow"
				}
			],
			"updated": 1713869381251,
			"link": null,
			"locked": false,
			"text": "urls.py",
			"rawText": "urls.py",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "urls.py",
			"lineHeight": 1.25
		},
		{
			"id": "8sF27Z48",
			"type": "text",
			"x": -43.26025658758755,
			"y": -10.037902236393052,
			"width": 73.5399169921875,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1155399754,
			"version": 183,
			"versionNonce": 1858301258,
			"isDeleted": false,
			"boundElements": [
				{
					"id": "L5auT1iYXSWw9WO34OL46",
					"type": "arrow"
				},
				{
					"id": "W7rm0PpQ8eDvXOs6NEpW7",
					"type": "arrow"
				},
				{
					"id": "mWfpLEcrppvqOBljJXQOO",
					"type": "arrow"
				},
				{
					"id": "rcHI6cKfWG_n3mKBEflqy",
					"type": "arrow"
				}
			],
			"updated": 1713869385009,
			"link": null,
			"locked": false,
			"text": "views.py",
			"rawText": "views.py",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "views.py",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 146,
			"versionNonce": 206275094,
			"isDeleted": false,
			"id": "oTqH8ZRX",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -41.471500396728516,
			"y": 101.6322250366211,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 73.5399169921875,
			"height": 25,
			"seed": 130029770,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "CpmVXFxfAu3B1v_OgHWUU",
					"type": "arrow"
				},
				{
					"id": "S-VUP-RJ3BQRTi4ZuSgz2",
					"type": "arrow"
				},
				{
					"id": "rcHI6cKfWG_n3mKBEflqy",
					"type": "arrow"
				}
			],
			"updated": 1713869381251,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "views.py",
			"rawText": "views.py",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "views.py",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 360,
			"versionNonce": 1370870346,
			"isDeleted": false,
			"id": "lLotnkwY",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 134.71448348663898,
			"y": -7.895301128933852,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 60.7999267578125,
			"height": 25,
			"seed": 210305290,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "mWfpLEcrppvqOBljJXQOO",
					"type": "arrow"
				},
				{
					"id": "qzmpGRUcyrYYRqkCjB8ZN",
					"type": "arrow"
				}
			],
			"updated": 1713869381251,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "urls.py",
			"rawText": "urls.py",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "urls.py",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 285,
			"versionNonce": 721342294,
			"isDeleted": false,
			"id": "Sq6EZcxf",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 139.34331272535394,
			"y": 103.67545388270136,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 60.7999267578125,
			"height": 25,
			"seed": 867818506,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "rcHI6cKfWG_n3mKBEflqy",
					"type": "arrow"
				},
				{
					"id": "2Jsg1BMtcjlpaWqUYdnRv",
					"type": "arrow"
				}
			],
			"updated": 1713869381251,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "urls.py",
			"rawText": "urls.py",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "urls.py",
			"lineHeight": 1.25
		},
		{
			"id": "L5auT1iYXSWw9WO34OL46",
			"type": "arrow",
			"x": -136.3018004377889,
			"y": -4.75493984052778,
			"width": 80.77410036933584,
			"height": 3.0695184433685654,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 292085782,
			"version": 121,
			"versionNonce": 1787568842,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713869385009,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					80.77410036933584,
					3.0695184433685654
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "saKJc2whkXn23ykuVD-Fx",
				"focus": 0.052004524571089304,
				"gap": 9.055533699173992
			},
			"endBinding": {
				"elementId": "8sF27Z48",
				"focus": 0.16435096997454385,
				"gap": 12.267443480865495
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "CpmVXFxfAu3B1v_OgHWUU",
			"type": "arrow",
			"x": -125.23693727912797,
			"y": 116.42429164979987,
			"width": 74.22684185115457,
			"height": 1.7350119734006455,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1127785110,
			"version": 257,
			"versionNonce": 1213625494,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713869381251,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					74.22684185115457,
					-1.7350119734006455
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "dvUGt3QLEYjoNiExLJohc",
				"focus": 0.2096027632904597,
				"gap": 11.704482566517925
			},
			"endBinding": {
				"elementId": "oTqH8ZRX",
				"focus": 0.039326579244177394,
				"gap": 9.538595031244881
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "W7rm0PpQ8eDvXOs6NEpW7",
			"type": "arrow",
			"x": 41.75094002991182,
			"y": -1.9525216000514476,
			"width": 80.07852332050007,
			"height": 75.00696131413466,
			"angle": 0,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 891326294,
			"version": 156,
			"versionNonce": 281376842,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713869385009,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					80.07852332050007,
					-75.00696131413466
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "8sF27Z48",
				"focus": 0.8681260144796962,
				"gap": 11.471279625311865
			},
			"endBinding": {
				"elementId": "TtPBKwYC",
				"focus": 0.556878190250716,
				"gap": 10.979975871309307
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "S-VUP-RJ3BQRTi4ZuSgz2",
			"type": "arrow",
			"x": 42.11920382084776,
			"y": 112.01759667635801,
			"width": 91.51556016115734,
			"height": 178.35273608387263,
			"angle": 0,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1240231818,
			"version": 168,
			"versionNonce": 1040675286,
			"isDeleted": false,
			"boundElements": [],
			"updated": 1713869381251,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					91.51556016115734,
					-178.35273608387263
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "oTqH8ZRX",
				"focus": 1.0590913319111899,
				"gap": 10.050787225388774
			},
			"endBinding": {
				"elementId": "TtPBKwYC",
				"focus": 0.42702650508199,
				"gap": 13.663800106645539
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "mWfpLEcrppvqOBljJXQOO",
			"type": "arrow",
			"x": 38.523671110256544,
			"y": -0.13690976138106992,
			"width": 86.12102193074145,
			"height": 5.541150408003899,
			"angle": 0,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1532176790,
			"version": 268,
			"versionNonce": 902262218,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713869385009,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					86.12102193074145,
					5.541150408003899
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "8sF27Z48",
				"focus": -0.36968611256008294,
				"gap": 8.244010705656592
			},
			"endBinding": {
				"elementId": "lLotnkwY",
				"focus": -0.23543367932925166,
				"gap": 10.06979044564099
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "rcHI6cKfWG_n3mKBEflqy",
			"type": "arrow",
			"x": 40.73607876096929,
			"y": 114.79234598786874,
			"width": 90.82417091923753,
			"height": 1.844143859776807,
			"angle": 0,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 199828234,
			"version": 206,
			"versionNonce": 417160522,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713869381251,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					90.82417091923753,
					-1.844143859776807
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "oTqH8ZRX",
				"focus": 0.11948059639360839,
				"gap": 8.667662165510308
			},
			"endBinding": {
				"elementId": "Sq6EZcxf",
				"focus": 0.3051354966093801,
				"gap": 7.7830630451471166
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "qzmpGRUcyrYYRqkCjB8ZN",
			"type": "arrow",
			"x": 161.52760456662605,
			"y": -18.908152506134144,
			"width": 0.9220719298884319,
			"height": 49.33086541407583,
			"angle": 0,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1830721750,
			"version": 219,
			"versionNonce": 1445080662,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713869393351,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.9220719298884319,
					-49.33086541407583
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "lLotnkwY",
				"focus": -0.10274173014695728,
				"gap": 11.012851377200292
			},
			"endBinding": {
				"elementId": "TtPBKwYC",
				"focus": 0.08579643404470096,
				"gap": 11.759921593950182
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "2Jsg1BMtcjlpaWqUYdnRv",
			"type": "arrow",
			"x": 203.5398837774046,
			"y": 94.48489353616782,
			"width": 24.795498076755337,
			"height": 165.15346497174284,
			"angle": 0,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1781538890,
			"version": 274,
			"versionNonce": 218921110,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1713869395541,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					18.876453480843736,
					-82.95374512778619
				],
				[
					-5.9190445959116005,
					-165.15346497174284
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "Sq6EZcxf",
				"focus": 0.8681415572120925,
				"gap": 9.190560346533545
			},
			"endBinding": {
				"elementId": "TtPBKwYC",
				"focus": -0.8268923037994649,
				"gap": 9.330368078585138
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "SAVLg51e",
			"type": "text",
			"x": 76.53780457114604,
			"y": 8.93252017405257,
			"width": 10,
			"height": 25,
			"angle": 0,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 955688534,
			"version": 3,
			"versionNonce": 499236490,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1713869381251,
			"link": null,
			"locked": false,
			"text": "",
			"rawText": "",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "S-VUP-RJ3BQRTi4ZuSgz2",
			"originalText": "",
			"lineHeight": 1.25
		}
	],
	"appState": {
		"theme": "dark",
		"viewBackgroundColor": "transparent",
		"currentItemStrokeColor": "#e03131",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 463.8927223882348,
		"scrollY": 197.8255208333984,
		"zoom": {
			"value": 1.6085872203750151
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%