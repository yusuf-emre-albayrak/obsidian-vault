---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
HTML
Css
JS ^Pk48kQQe

views.py ^xwF1YPI5

urls.py ^G1OMk6Wd

models ^Sq4gEx8T

database ^mn0UawGp

user ^0qzsaelF

View ^iH57pv3x

Model ^yuoT7491

Template ^nGhQopIW

Backend ^VNiFm4CS

Frontend ^iFNYWp2L

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/2.1.4",
	"elements": [
		{
			"type": "rectangle",
			"version": 375,
			"versionNonce": 1920833674,
			"isDeleted": false,
			"id": "89a6vGskouxXQXRF4odf_",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -512.2736322381216,
			"y": -280.13670207843825,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#b2f2bb",
			"width": 150.81964111328125,
			"height": 112.45901489257812,
			"seed": 1780579030,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"type": "text",
					"id": "Pk48kQQe"
				},
				{
					"id": "BWLiin8HAZWa94Fod8BVC",
					"type": "arrow"
				},
				{
					"id": "3GPq738CbVofKrEdyLFJo",
					"type": "arrow"
				}
			],
			"updated": 1713876545982,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 318,
			"versionNonce": 808291606,
			"isDeleted": false,
			"id": "Pk48kQQe",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -464.06379337093415,
			"y": -261.4071946321492,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 54.39996337890625,
			"height": 75,
			"seed": 250048906,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713876545982,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "HTML\nCss\nJS",
			"rawText": "HTML\nCss\nJS",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "89a6vGskouxXQXRF4odf_",
			"originalText": "HTML\nCss\nJS",
			"lineHeight": 1.25
		},
		{
			"type": "rectangle",
			"version": 523,
			"versionNonce": 804018570,
			"isDeleted": false,
			"id": "CFYa8Hl0kJPdNvIV5hdYz",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -318.70107248694524,
			"y": -247.5314604779096,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffc9c9",
			"width": 128.7440671584274,
			"height": 62.13516915525932,
			"seed": 551778506,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"type": "text",
					"id": "xwF1YPI5"
				},
				{
					"id": "F_JzXLZNPLG6ySjUr23Ah",
					"type": "arrow"
				},
				{
					"id": "1SUm_XvgQ2Pf-LpoToDlF",
					"type": "arrow"
				},
				{
					"id": "3GPq738CbVofKrEdyLFJo",
					"type": "arrow"
				}
			],
			"updated": 1713876545982,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 476,
			"versionNonce": 1013414486,
			"isDeleted": false,
			"id": "xwF1YPI5",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -291.09899740382525,
			"y": -228.96387590027996,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 73.5399169921875,
			"height": 25,
			"seed": 1149913034,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713876545982,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "views.py",
			"rawText": "views.py",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "CFYa8Hl0kJPdNvIV5hdYz",
			"originalText": "views.py",
			"lineHeight": 1.25
		},
		{
			"type": "rectangle",
			"version": 528,
			"versionNonce": 1455905814,
			"isDeleted": false,
			"id": "k9hyFHaR8kqzqfDrISVLv",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -317.12033939651377,
			"y": -135.48946642493206,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffc9c9",
			"width": 126.25864373586779,
			"height": 64.62056481723684,
			"seed": 1737598474,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"type": "text",
					"id": "G1OMk6Wd"
				},
				{
					"id": "F_JzXLZNPLG6ySjUr23Ah",
					"type": "arrow"
				}
			],
			"updated": 1713876545982,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 498,
			"versionNonce": 493993878,
			"isDeleted": false,
			"id": "G1OMk6Wd",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -284.3909809074861,
			"y": -115.67918401631364,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 60.7999267578125,
			"height": 25,
			"seed": 2135184586,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713876545982,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "urls.py",
			"rawText": "urls.py",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "k9hyFHaR8kqzqfDrISVLv",
			"originalText": "urls.py",
			"lineHeight": 1.25
		},
		{
			"type": "arrow",
			"version": 462,
			"versionNonce": 2067541194,
			"isDeleted": false,
			"id": "F_JzXLZNPLG6ySjUr23Ah",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -257.3068235081755,
			"y": -176.94591460993365,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 0.6602866410058255,
			"height": 33.35399484775715,
			"seed": 1137778518,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1713876545982,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "CFYa8Hl0kJPdNvIV5hdYz",
				"gap": 8.450376712716647,
				"focus": 0.033741101250761255
			},
			"endBinding": {
				"elementId": "k9hyFHaR8kqzqfDrISVLv",
				"gap": 8.102453337244413,
				"focus": -0.07489721334243174
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-0.6602866410058255,
					33.35399484775715
				]
			]
		},
		{
			"type": "rectangle",
			"version": 580,
			"versionNonce": 125918422,
			"isDeleted": false,
			"id": "sI_yiWRfv3pkSRG3yceq7",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 23.722768629370705,
			"y": -253.605860585038,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 139.67988580075854,
			"height": 60,
			"seed": 1592176138,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"type": "text",
					"id": "mn0UawGp"
				},
				{
					"id": "POytq4NaGoKt1zkj1JedD",
					"type": "arrow"
				}
			],
			"updated": 1713876545982,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 579,
			"versionNonce": 829424522,
			"isDeleted": false,
			"id": "mn0UawGp",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 46.232747845667944,
			"y": -236.105860585038,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 94.65992736816406,
			"height": 25,
			"seed": 1454922954,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713876545982,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "database",
			"rawText": "database",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "sI_yiWRfv3pkSRG3yceq7",
			"originalText": "database",
			"lineHeight": 1.25
		},
		{
			"type": "rectangle",
			"version": 619,
			"versionNonce": 1613451798,
			"isDeleted": false,
			"id": "vvWSZHysSeb-WUu_hI_zp",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -147.35519515606845,
			"y": -252.41396838650473,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#a5d8ff",
			"width": 128.7440671584274,
			"height": 60,
			"seed": 1980862102,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"type": "text",
					"id": "Sq4gEx8T"
				},
				{
					"id": "1SUm_XvgQ2Pf-LpoToDlF",
					"type": "arrow"
				},
				{
					"id": "POytq4NaGoKt1zkj1JedD",
					"type": "arrow"
				}
			],
			"updated": 1713876545982,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 616,
			"versionNonce": 142279242,
			"isDeleted": false,
			"id": "Sq4gEx8T",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -114.07312739716724,
			"y": -234.91396838650473,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 62.179931640625,
			"height": 25,
			"seed": 2035239894,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713876545982,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "models",
			"rawText": "models",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "vvWSZHysSeb-WUu_hI_zp",
			"originalText": "models",
			"lineHeight": 1.25
		},
		{
			"type": "arrow",
			"version": 1071,
			"versionNonce": 308584278,
			"isDeleted": false,
			"id": "1SUm_XvgQ2Pf-LpoToDlF",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -178.02702471998495,
			"y": -217.24674772877108,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 19.497436479009224,
			"height": 0.17051273509451903,
			"seed": 766199562,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1713876545982,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "CFYa8Hl0kJPdNvIV5hdYz",
				"gap": 11.929980608532873,
				"focus": -0.04577878495794513
			},
			"endBinding": {
				"elementId": "vvWSZHysSeb-WUu_hI_zp",
				"gap": 11.17439308490728,
				"focus": -0.19626426243691675
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					19.497436479009224,
					0.17051273509451903
				]
			]
		},
		{
			"type": "arrow",
			"version": 1911,
			"versionNonce": 725100810,
			"isDeleted": false,
			"id": "POytq4NaGoKt1zkj1JedD",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -11.890568676803525,
			"y": -221.85310592147067,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 27.898616019134796,
			"height": 0.262892780808329,
			"seed": 1771123478,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1713876545982,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "vvWSZHysSeb-WUu_hI_zp",
				"gap": 6.720559320837509,
				"focus": -0.003535406508011457
			},
			"endBinding": {
				"elementId": "sI_yiWRfv3pkSRG3yceq7",
				"gap": 7.714721287039424,
				"focus": -0.08958335382120819
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					27.898616019134796,
					0.262892780808329
				]
			]
		},
		{
			"type": "ellipse",
			"version": 527,
			"versionNonce": 1652704406,
			"isDeleted": false,
			"id": "DUgsi7qoCPbimOcWHZVmb",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -654.5481041420549,
			"y": -275.378653565186,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "#ffec99",
			"width": 103.18573058607103,
			"height": 97.23271050917822,
			"seed": 1943321366,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [
				{
					"type": "text",
					"id": "0qzsaelF"
				},
				{
					"id": "BWLiin8HAZWa94Fod8BVC",
					"type": "arrow"
				}
			],
			"updated": 1713876545982,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 494,
			"versionNonce": 824342474,
			"isDeleted": false,
			"id": "0qzsaelF",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -623.8168781238032,
			"y": -239.13925278769113,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 41.75994873046875,
			"height": 25,
			"seed": 717298326,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713876545982,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "user",
			"rawText": "user",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "DUgsi7qoCPbimOcWHZVmb",
			"originalText": "user",
			"lineHeight": 1.25
		},
		{
			"type": "arrow",
			"version": 1469,
			"versionNonce": 1111048662,
			"isDeleted": false,
			"id": "BWLiin8HAZWa94Fod8BVC",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -538.7771442641516,
			"y": -225.48203731914657,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 12.772905079329234,
			"height": 0.397772200714428,
			"seed": 668258262,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1713876545982,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "DUgsi7qoCPbimOcWHZVmb",
				"gap": 12.599269442706927,
				"focus": 0.06346230369752116
			},
			"endBinding": {
				"elementId": "89a6vGskouxXQXRF4odf_",
				"gap": 13.730606946700732,
				"focus": 0.08106494074949215
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					12.772905079329234,
					-0.397772200714428
				]
			]
		},
		{
			"type": "arrow",
			"version": 606,
			"versionNonce": 173338250,
			"isDeleted": false,
			"id": "3GPq738CbVofKrEdyLFJo",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -348.074960420093,
			"y": -221.0968475771151,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 19.912951667367338,
			"height": 0.042286089804122184,
			"seed": 984643478,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1713876545982,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "89a6vGskouxXQXRF4odf_",
				"gap": 13.379030704747379,
				"focus": 0.053176203262414096
			},
			"endBinding": {
				"elementId": "CFYa8Hl0kJPdNvIV5hdYz",
				"gap": 9.460936265780475,
				"focus": 0.15485200431643734
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					19.912951667367338,
					-0.042286089804122184
				]
			]
		},
		{
			"type": "text",
			"version": 70,
			"versionNonce": 614373142,
			"isDeleted": false,
			"id": "iH57pv3x",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -270.49552642358935,
			"y": -41.75262160555462,
			"strokeColor": "#e03131",
			"backgroundColor": "#ffec99",
			"width": 37.8399658203125,
			"height": 25,
			"seed": 1884113110,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713876545982,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "View",
			"rawText": "View",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "View",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 142,
			"versionNonce": 1804242250,
			"isDeleted": false,
			"id": "yuoT7491",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -107.88729626755958,
			"y": -164.49488801414913,
			"strokeColor": "#1971c2",
			"backgroundColor": "#ffec99",
			"width": 53.979949951171875,
			"height": 25,
			"seed": 62169930,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713876545982,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Model",
			"rawText": "Model",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Model",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 76,
			"versionNonce": 1378305110,
			"isDeleted": false,
			"id": "nGhQopIW",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -484.0632038041169,
			"y": -125.64923311906699,
			"strokeColor": "#2f9e44",
			"backgroundColor": "#ffec99",
			"width": 90.37991333007812,
			"height": 25,
			"seed": 1790110166,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713876545982,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Template",
			"rawText": "Template",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Template",
			"lineHeight": 1.25
		},
		{
			"type": "line",
			"version": 242,
			"versionNonce": 975066122,
			"isDeleted": false,
			"id": "m7hFoktdrHmwEyQlVqA1N",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "dotted",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -339.89915751376896,
			"y": -316.7208205414606,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 306.6290468623703,
			"seed": 1605705814,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1713876545982,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					306.6290468623703
				]
			]
		},
		{
			"type": "text",
			"version": 78,
			"versionNonce": 1017661846,
			"isDeleted": false,
			"id": "VNiFm4CS",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "dotted",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -173.01094093087647,
			"y": -350.90358231595104,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 79.3199462890625,
			"height": 25,
			"seed": 288689046,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713876545982,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Backend",
			"rawText": "Backend",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Backend",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 138,
			"versionNonce": 2014003914,
			"isDeleted": false,
			"id": "iFNYWp2L",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "dotted",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -553.8130729597051,
			"y": -347.4008144114751,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 83.45993041992188,
			"height": 25,
			"seed": 576965834,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1713876545982,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Frontend",
			"rawText": "Frontend",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Frontend",
			"lineHeight": 1.25
		},
		{
			"type": "line",
			"version": 205,
			"versionNonce": 1398187734,
			"isDeleted": false,
			"id": "PXQ4KipqZxMrncDsMhl-2",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "dotted",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -654.6928777426028,
			"y": -315.81363461185356,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 826.4469222858559,
			"height": 0,
			"seed": 2113569814,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1713876545982,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					826.4469222858559,
					0
				]
			]
		},
		{
			"id": "ZigwXA4o5ie7gzu_psEzY",
			"type": "arrow",
			"x": -181.81384497212105,
			"y": -219.9546070084283,
			"width": 17.678741500547005,
			"height": 0,
			"angle": 0,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "dotted",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 102803606,
			"version": 16,
			"versionNonce": 565329546,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1713876545333,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					17.678741500547005,
					0
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "k9hyFHaR8kqzqfDrISVLv",
				"focus": 0.06666453201808735,
				"gap": 9.047850688524932
			},
			"endBinding": {
				"elementId": "CFYa8Hl0kJPdNvIV5hdYz",
				"focus": 0.11235926949602168,
				"gap": 7.7565913899946395
			},
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"type": "arrow",
			"version": 1251,
			"versionNonce": 703718230,
			"isDeleted": true,
			"id": "DitznsOaFq36mcQO3ovZv",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -8.50932078243768,
			"y": -206.4484508848105,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 19.497436479009227,
			"height": 0.17095053408479544,
			"seed": 1203648074,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1713876545542,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					19.497436479009227,
					0.17095053408479544
				]
			]
		},
		{
			"type": "arrow",
			"version": 1270,
			"versionNonce": 1495483338,
			"isDeleted": true,
			"id": "-jFEXvd1aB9cG7QtsZasH",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -172.4835700424139,
			"y": -196.62151134332237,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 14.329241382290178,
			"height": 1.0323110385852772,
			"seed": 1033618186,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1713876544733,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					14.329241382290178,
					1.0323110385852772
				]
			]
		}
	],
	"appState": {
		"theme": "dark",
		"viewBackgroundColor": "transparent",
		"currentItemStrokeColor": "#1971c2",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "dotted",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "center",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": null,
		"scrollX": 901.7594640891531,
		"scrollY": 647.5243430087231,
		"zoom": {
			"value": 0.9491573219449627
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%