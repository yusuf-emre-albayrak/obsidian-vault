# have got, has got
- i
- we
- you

gibi ifadelerde **have got** kullanılır,
- he
- she
- it

gibi ifadelerede ise **has got** kullanılır.

## Soru Yapımı
**have** + ..... + **got** ?
**has**   + ..... + **got** ?

## Have got / Have
**have got** ile **have** aynı şeydir farkları olumsuz ve soru cümlelerinde ortaya çıkar.
Have daha sık kullanılır.

>[!example] Örnek
>**Do** you **have** ...
>**Have** you **got** ....?

>[!example] Örnek
>**Does** he **have** ...
>**Has** he **got** ....?

# There is  / are
## geçmiş zaman yapımı
çok basittir direk 
- is -> *was*
- are -> *were* 

getirilir ve biter.
>[!example] Örnek
>There was a problem.
>*Bir problem vardı.*

# cümle kurulumu
özne + fiil.
özne + fiil + obje
özne + fiil + obje + yer
özne + fiil + obje + yer + zaman
 
# Present Simple (Geniş Zaman)
geniş zaman, yapar eder gibi şeyler için kullanılır.

## Yapı


|         Olumlu         | yapılar                                   |
| :--------------------: | ----------------------------------------- |
| i<br>you<br>we<br>they | play<br>go<br>cook<br>watch               |
|    he<br>she <br>it    | play*s*<br>go*es*<br>cook*s*<br>watch*es* |



**Özne** > **Fiil** > **Obje - Yer - Zaman**

>[!example]- Örnekler
>He *gets up* early every day / Her gün erken kalkar.
>He *lives* in Spain / İspanya'da yaşar.
>Water *boils* aat 100C / Su yüz derecede kaynar.

## Kullanımı
- Genel - Tekrarlanan işlemler
- Alışkanlıklar
- Bilimsel gerçekler
- Otobüs / Tren vs programları
- Gazete başlıkları

# Present continuous / şimdiki zaman
## Kullanımı
- Konuşma anında gerçekleşen işlemler
- Geçici durumlar
- Yakın gelecek zaman
- Şikayet, yakınma (always)

>[!example]- Örnekler
>I *am* watch*ing* a video now.
>He *is* fly*ing* with his sister.
>We *are* fly*ing* to Ankara tomorrow.
>You *are* always forgett*ing* your glasses.


| I                 | AM  | Watch*ing*              |
| ----------------- | --- | ----------------------- |
| We<br>You<br>They | ARE | Cook*ing*<br>Runn*ing*  |
| He<br>She<br>It   | IS  | Sleep*ing*<br>Walk*ing* |
