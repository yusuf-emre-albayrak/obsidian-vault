- her hafta *çarşamba 8:00* da teslim edilecek
- **sınıflandırma yöntemleri** kullanılarak çalışma yapılacak

# Makale araştırması
## Siteler
- google scholar
- pubmed
- IEEE Xplorer
- Science Direct
- SpringerLink
- Web of Science
- JSTOR

## Anahtar kelime belirleme
- [[Sınıflandırma Algoritmaları]]
- [[Makine öğrenimi]]
- [[Görüntü işleme]]
- [[Veri madenciliği]]


>[!attention] Makalelerde bulunması gereken ibareler!
>- SCI
>- SCI-E
>- Scopus
>- Tr dizin
>*makalelerde bu dizinlerden biri olduğu takdirde araştırmaya dahil edilebilir*

## Dikkat edilmesi gereken hususlar
- Her bir makale özeti en az 6 satırdan oluşmalıdır
- Her makale özetinde ilgili konu neden seçildiğine , nasıl bir model geliştirildiğine ve modelin performans sonuçlarına ilişkin bilgi verilmelidir
- Ayrıca, o makalede, ne tur bir açık var, tespit edilmelidir.
- her makale sonunda referans numarası yer almalı. Örneğin [1] şeklinde
- Kaynaklar (References) ile ilgili başlık makaleler özetlerinin sonunda yer almalı. Her referans numaralar ile yazılmalı aşağıdaki şekilde. *[1] Cicek, G., & Akan, A. (2023, October). Machine and Deep Learning Based Detection of Attention Deficit Hyperactivity Disorder. In 2023 Innovations in Intelligent Systems and Applications Conference (ASYU) (pp. 1-6). IEEE*

**Makale özetlerini çıkarmadaki amacınız kendi konunuzu belirlemek olduğundan, çalışmaları incelerken, ilginizi çeken makaleleri ayrı bir klasöre alarak, daha sonrasında detaylı okuyunuz**



# Veri seti

## Siteler
- kaggle
- UCI

## Dikkat edilmesi gereken hususlar
- Veri seti dengeli olması
- yeterince örnek olması