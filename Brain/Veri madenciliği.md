> [!Wikipedia]
> **Veri madenciliği**, büyük ölçekli veriler arasından faydalı bilgiye ulaşma, bilgiyi madenleme işidir. Büyük veri yığınları içerisinden gelecekle ilgili tahminde bulunabilmemizi sağlayabilecek bağıntıların bilgisayar programı kullanarak aranması olarak da tanımlanabilir.
>
> [Wikipedia](https://tr.wikipedia.org/wiki/Veri%20madencili%C4%9Fi)